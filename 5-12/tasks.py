
# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
os.system("taskset -p 0xff %d" % os.getpid())
from celery import Celery
 

import requests
import time
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchWindowException, InvalidSessionIdException, WebDriverException, JavascriptException

from pyvirtualdisplay import Display

import psycopg2
import pandas as pd
from datetime import datetime, timedelta
import base64
import psutil


app = Celery('tasks', broker='pyamqp://guest@localhost//')
# app = Celery('tasks', backend='redis://localhost', broker='pyamqp://')


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)



def get_driver(url):


    options = Options()
    options.add_argument("user-data-dir=/opt/google/chrome")
    options.add_argument('--profile-directory=Default')
    options.add_argument("no-sandbox")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--headless')

    #options.add_argument("--disable-infobars")           # disabling infobars 
    #options.add_argument("--disable-extensions")        # disabling extensions
    options.add_argument("--disable-gpu")              # applicable to windows os only
    #options.add_argument("--disable-dev-shm-usage")    # overcome limited resource problems
    #options.add_argument("--no-sandbox")               # Bypass OS security model



    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver_3',
        desired_capabilities=caps,
        chrome_options = options,
        # options = options
    )
    driver.set_window_size(1920, 800)
    driver.maximize_window()
    driver.get(url)

    time.sleep(2)
    return driver





def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    look = soup.find('div', class_='heroes-result-total')
    if look:
        res = int(look.find('span').text.strip().replace(' ', ''))
    else:
        res = 0
    
    look2 = soup.find('ul', class_='pagination-list')
    if look2:
        try:
            pagg = look2.find_all('a')[-1].get('onclick')
            last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
        except IndexError as err:

            message = 'Imdex error in def get_total_pages'
            write_error_in_file(err, driver.page_source, message)
            if res < 10:
                last_page = 1
            else:
                print('Houston we have a problem. {}'.format(driver.current_url))
                get_total_pages(driver)

    return int(last_page), int(res)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


# GENERAL PHOTO
def get_image(soup, driver, ids):
    look = soup.find('div', class_='gi__img')
    if look:
        img_src = look.find('img').get('src') if look else ''
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# MAIN DOCUMENT PICT
def get_image_reward(soup, driver, ids):
    look = soup.find_all("img", {'class':'mapster_el'})
    if look:
        img_src = soup.find_all("img", {'class':'mapster_el'})[0].get('src')
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# PHOTO OF MEDAL
def get_image_medal(soup, driver, ids):
    look = soup.find_all("div", {'class':'c-reward__img'})
    if look:
        img_src = soup.find_all("div", {'class':'c-reward__img'})[0].find('img').get('src')
        if 'bitrix' in img_src:
            img_src = 'https://pamyat-naroda.ru'+img_src
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


def save_image_on_server(img_url, driver, ids):


    try:
        driver.get(img_url)

        base64img = driver.execute_script('''
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
                var widthScale = null;
                var heightScale = null;

                if (maxWidth != null)
                {
                    widthScale = maxWidth / srcWidth;
                }
                if (maxHeight != null)
                {
                    heightScale = maxHeight / srcHeight;
                }

                var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
                return {
                    width: Math.round(srcWidth * ratio),
                    height: Math.round(srcHeight * ratio)
                };
            }

            function getBase64FromImage(img, width, height) {
                var size = getMaxSize(width, height, 600, 600)
                canvas.width = size.width;
                canvas.height = size.height;
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, size.width, size.height);
                ctx.drawImage(img, 0, 0, size.width, size.height);
                return canvas.toDataURL('image/jpeg', 0.9);
            }
            img = document.getElementsByTagName('img')[0]
            console.log(img)
            res = getBase64FromImage(img, img.width, img.height)
            return res
        ''')

        convert_str_for_encoding = base64img.split('base64,')[1]
        imgdata = base64.b64decode(convert_str_for_encoding)

        old_name = img_url.split('/')[-1].replace('.','-')
        path = os.path.dirname(os.path.abspath(__file__))+'/images/{}-{}.jpg'.format(ids, old_name)
        f = open(path, 'wb')
        f.write(imgdata)
        f.close()
        return path
    except WebDriverException as err:
        message = 'Error convert to base64 - no webdriver'
        write_error_in_file(err,driver.page_source, message)
        print(message)

        return ''


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


def get_inner_info(bsd, url, cursor, conn, driver, pid, pid2):

    soup = BeautifulSoup(bsd, 'html.parser')


    name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
    
    name = name_full.split(' ')[0]
    second_name = name_full.split(' ')[-1]
    surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
    zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
    birth = get_value_from_field(soup, 'Дата рождения')
    voin_4astj = get_value_from_field(soup, 'Воинская часть')
    poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
    mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
    mesto_priziva = get_value_from_field(soup, 'Место призыва')
    data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
    data_smerti = get_value_from_field(soup, 'Дата смерти')
    some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
    page_url = str(url)
    komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''


    # REWARD
    naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
    data_podviga = get_value_from_field(soup, 'Даты подвига')
    data_vibitija = get_value_from_field(soup, 'Дата выбытия')
    pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
    mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
    kto_nagradil = get_value_from_field(soup, 'Кто наградил')
    isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
    nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
    nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
    nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
    archive = get_value_from_field(soup, 'Архив')
    mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
    nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
    tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
    nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
    data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
    prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
    prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
    izdan = get_reward_info(soup, 'Издан:')
    fond = get_reward_info(soup, 'Фонд:')
    opisj = get_reward_info(soup, 'Опись:')
    edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
    nomer_zapisi = get_reward_info(soup, '№ записи:')


    if 'chelovek_nagrazhdenie' in str(url):
        ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
    elif '_donesenie' in str(url):
        ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
    elif 'chelovek_zahoronenie' in str(url):
        ids = re.findall(r"(?<=chelovek_zahoronenie)[^\/]+", str(url))[0]
    elif 'chelovek_kartoteka' in str(url):
        ids = re.findall(r"(?<=chelovek_kartoteka_memorial)[^\/]+", str(url))[0]
    elif 'chelovek_gospital' in str(url):
        ids = re.findall(r"(?<=chelovek_gospital)[^\/]+", str(url))[0]
    elif '_knigi_pamyati' in str(url):
        ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
    elif 'chelovek_vpp' in str(url):
        ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
    else:
        ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
        full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
        name = full_nm.split(' ')[1]
        second_name = full_nm.split(' ')[-1]
        surname = full_nm.split(' ')[0]



    #try:
    #    image_reward = get_image_reward(soup, driver, ids)
    #except NoSuchWindowException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_reward = get_image_reward(soup, driver, ids)
    #except WebDriverException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_reward = get_image_reward(soup, driver, ids)

    #try:
    #    image_general = get_image(soup, driver, ids)
    #except NoSuchWindowException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_general = get_image(soup, driver, ids)
    #except WebDriverException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_general = get_image(soup, driver, ids)

    #try:
    #    image_medal = get_image_medal(soup, driver, ids)
    #except NoSuchWindowException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_medal = get_image_medal(soup, driver, ids)
    #except WebDriverException:
    #    time.sleep(1)
    #    driver = get_driver(url)
    #    time.sleep(2)
    #    image_medal = get_image_medal(soup, driver, ids)


    try:
        image_reward = get_image_reward(soup, driver, ids)
    except JavascriptException as err:
        write_error_in_file(err, url)
        image_reward = ''

    try:
        image_general = get_image(soup, driver, ids)
    except JavascriptException: 
        write_error_in_file(err, url) 
        image_general = ''

    try:
        image_medal = get_image_medal(soup, driver, ids)
    except JavascriptException:    
        write_error_in_file(err, url)
        image_medal = ''


    # insert data in query
    insert_query = """ INSERT INTO parsed_data 
                (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                
                
                naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                izdan, fond, opisj, edinica_hranenija, nomer_zapisi) 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                        %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                """
    record_to_insert = (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                        poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                        data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                        
                        naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                        kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                        nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                        nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                        izdan, fond, opisj, edinica_hranenija, nomer_zapisi)
    cursor.execute(insert_query, record_to_insert)
    conn.commit()


    # update data in query
    insert_query2 = """Update pages_in_use set parsed = true where url_current_inner = %s"""

    cursor.execute(insert_query2, (url,))
    conn.commit()

    cursor.close()
    conn.close()

    # 5 сек мало - ошибки
    time.sleep(15)
    # kill pid1
    p = psutil.Process(pid)
    p.kill()

    # kill pid2
    for i in pid2:
        try:
            i.kill()
        except psutil.NoSuchProcess as err:
            message = 'No PID for process'
            write_error_in_file(err,url, message)
 
    #try:
    #    driver.close()
    #except NoSuchWindowException as e:
    #    print('Driver is already closed')


def save_pid(driver):
    pid = driver.service.process.pid
    p = psutil.Process(driver.service.process.pid)
    pid2 = p.children(recursive=True)

    pid2_g = [i.pid for i in pid2]
    pid2_g.append(pid)

    return pid, pid2, pid2_g


@app.task
def get_inner_info_task(url):

    conn = psycopg2.connect(
                database = "kniga2", 
                user = "delme", 
                password = "zaza1234", 
                host = "localhost", 
                port = "5432")
    cursor = conn.cursor()



    try:
        driver = get_driver(url)
    except WebDriverException as err:
        message = 'Message: unknown error: Chrome failed to start: exited abnormally'
        print(message)
        write_error_in_file(err,url, message)
        driver = get_driver(url)
        time.sleep(2)

    try:
        if not driver:
            driver = get_driver(url)

        pid, pid2, pid2_g = save_pid(driver)
        insert_query = """ INSERT INTO pids 
                (pid, added) VALUES (%s,%s)
                """
        record_to_insert = (sorted(pid2_g), datetime.now())
        cursor.execute(insert_query, record_to_insert)
        conn.commit()

        get_inner_info(driver.page_source, url, cursor, conn, driver, pid, pid2)
    except WebDriverException as err:

        message = 'Some problem with chromedriver'
        write_error_in_file(err, url)
        print(message)
        driver = get_driver(url)
        time.sleep(5)
        #pid = driver.service.process.pid

        pid, pid2, pid2_g = save_pid(driver)
        insert_query = """ INSERT INTO pids (pid, added) VALUES (%s,%s) """
        record_to_insert = (sorted(pid2_g), datetime.now())
        cursor.execute(insert_query, record_to_insert)
        conn.commit()

        get_inner_info(driver.page_source, url, cursor, conn, driver, pid, pid2)
        
    except NoSuchWindowException as err:
        
        message = 'Main driver not worked from tasks'
        write_error_in_file(err, url)
        print(message)
        driver = get_driver(url)
        time.sleep(5)
        pid = driver.service.process.pid

        pid, pid2, pid2_g = save_pid(driver)
        insert_query = """ INSERT INTO pids (pid, added) VALUES (%s,%s) """
        record_to_insert = (sorted(pid2_g), datetime.now())
        cursor.execute(insert_query, record_to_insert)
        conn.commit()

        get_inner_info(driver.page_source, url, cursor, conn, driver, pid, pid2)

    except InvalidSessionIdException as err:

        message = 'NO session ID'
        write_error_in_file(err, url)
        print(message)

        driver = get_driver(url)
        time.sleep(5)
        pid = driver.service.process.pid

        pid, pid2, pid2_g = save_pid(driver)
        insert_query = """ INSERT INTO pids (pid, added) VALUES (%s,%s) """
        record_to_insert = (sorted(pid2_g), datetime.now())
        cursor.execute(insert_query, record_to_insert)
        conn.commit()

        get_inner_info(driver.page_source, url, cursor, conn, driver, pid, pid2)


    #try:
    #    driver.close()
    #except NoSuchWindowException as e:
    #    print('Driver is already closed')



def write_error_in_file(err, url, message=False):
    f = open("stdout_error.txt", "a+")
    if message:
        f.write(message+'\n')
    f.write(str(datetime.now())+'  '+str(err))
    f.write(url)
    f.write('\n')
    f.close()


@app.task
def clean_pid():
    now = datetime.now()
    one_year = datetime.now() + timedelta(days=365)
    while now < one_year:
        conn = psycopg2.connect(
            database = "kniga2", 
            user = "delme", 
            password = "zaza1234", 
            host = "localhost", 
            port = "5432")
        cursor = conn.cursor()
        cursor.execute("select id,pid,added from pids where added <= NOW() - interval'2 minute'")
        
        rows = cursor.fetchall()
        ids = [ row[0] for row in rows ]
        for row in rows:
            for pid in row[1].replace('{','').replace('}', '').split(','):
                try:
                    p = psutil.Process(int(pid))
                    p.kill()
                except psutil.NoSuchProcess as err: # psutil.NoSuchProcess: psutil.NoSuchProcess no process found with pid
                    pass
                except psutil.AccessDenied: # psutil.AccessDenied (pid=29082)
                    pass
        cursor.execute("delete from pids where id = ANY (%s)", (ids,))
        conn.commit()
        cursor.close()
        conn.close()

        time.sleep(60*60*2) # run every two hours
