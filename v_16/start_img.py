
import os
os.system("taskset -p 0xff %d" % os.getpid())


#import requests
import time
import re

from pyvirtualdisplay import Display

import psycopg2
from datetime import datetime, timedelta
import base64
import psutil
import subprocess


def kill_process(process_name):
    lal = [p.info['pid'] for p in psutil.process_iter(attrs=['pid', 'name']) if process_name in p.info['name']]
    if lal:
        for pid in lal:
            try:
                #i.kill()
                p = psutil.Process(pid)
                p.kill()

            except psutil.NoSuchProcess as err:
                message = 'No PID for process'



now = datetime.now()
one_year = datetime.now() + timedelta(days=365)
while now < one_year:
    os.system("pkill -9 chrome")
    os.system("pkill -9 chromium-browser") 
    os.system("pkill -9 celery")
    os.system("pkill -9 tasks")    
    os.system("pkill -9 chromedriver_3")
    os.system("pkill chromium")

    
    print("*"*150)
    print("Start looping")
    os.system("while pgrep chrome ; do pkill chrome ; done")
    os.system("while pgrep chromedriver_3 ; do pkill chromedriver_3 ; done")
    os.system("while pgrep chromium-browser ; do pkill chromium-browser ; done")
    os.system("while pgrep chromium ; do pkill chromium ; done")
    print("Finish looping")
    print("*"*150)


    os.system("pkill -x chromium-browser")
    os.system("pkill -x chromium")
    os.system("pkill -x -15 chromium-browser")
    os.system("pkill -x -15 chromium")
    os.system("pkill -x celery")

    os.system("pkill chromium")
   
    kill_process("chrome")
    kill_process("chromedriver_3")
    kill_process("chromium-browser")
    kill_process("celery")
    kill_process("tasks")

    print("*"*150)
    print("remove tmp")
    os.system("rm -r /tmp/*")
    os.system("rm -r /RAID/tmp/*")
    os.system("find /RAID/tmp/ -not -path '*/\.*' -delete")
    os.system("mkdir /RAID/tmp/")

    print("remove tmp finish")
    print("*"*150)

    print("*"*150)
    print("start *.sh")
    os.system("sh run_single_parser.sh")
    print("finish *.sh")
    print("*"*150)


    #subprocess.call("run_parser.sh", shell=True)
    time.sleep(21)

