
import os
os.system("taskset -p 0xff %d" % os.getpid())
from celery import Celery
 

import requests
import time
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchWindowException, InvalidSessionIdException, WebDriverException, JavascriptException

from pyvirtualdisplay import Display

import psycopg2
import pandas as pd
from datetime import datetime, timedelta
import base64
import psutil
import subprocess



def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth={}&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_vpp&page=1'.format(date)


def get_driver(url):


    options = Options()
    options.add_argument("user-data-dir=/opt/google/chrome")
    options.add_argument('--profile-directory=Default')


    options.add_argument("no-sandbox")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--headless')
    

    options.add_argument("--disable-extensions")
    options.add_argument("--start-maximized")
    options.add_argument("--disable-infobars")
    options.add_experimental_option('useAutomationExtension', False)



    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/ch_drv/chromedriver_78_3',
        desired_capabilities=caps,
        chrome_options = options,
    )
     
    driver.set_window_size(1920, 800)
    driver.maximize_window()
    driver.get(url)

    time.sleep(2)
    return driver





def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    look = soup.find('div', class_='heroes-result-total')
    if look:
        res = int(look.find('span').text.strip().replace(' ', ''))
    else:
        res = 0
    
    look2 = soup.find('ul', class_='pagination-list')

    if look2:
        try:
            pagg = look2.find_all('a')[-1].get('onclick')
            last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
        except IndexError as err:

            message = 'Imdex error in def get_total_pages'
            write_error_in_file(err, message)
            if res < 10:
                last_page = 1
            else:
                print('Houston we have a problem. {}'.format(driver.current_url))
                get_total_pages(driver)

    return int(last_page), int(res)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


# GENERAL PHOTO
def get_image(soup, driver, ids):
    look = soup.find('div', class_='gi__img')
    if look:
        img_src = look.find('img').get('src') if look else ''
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# MAIN DOCUMENT PICT
def get_image_reward(soup, driver, ids):
    print('get_image_reward')
    look = soup.find_all("img", {'id':'image'})
    print(look)
    
    if look:
        img_src = soup.find_all("img", {'id':'image'})[0].get('src')
        res = save_image_on_server(img_src, driver, ids)
        print(f'get_image_reward   done    ----    {res}')
        return res
    return ''


# PHOTO OF MEDAL
def get_image_medal(soup, driver, ids):
    look = soup.find_all("div", {'class':'c-reward__img'})
    if look:
        img_src = soup.find_all("div", {'class':'c-reward__img'})[0].find('img').get('src')
        if 'bitrix' in img_src:
            img_src = 'https://pamyat-naroda.ru'+img_src
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


def save_image_on_server(img_url, driver, ids):


    try:
        driver.get(img_url)

        base64img = driver.execute_script('''
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
                var widthScale = null;
                var heightScale = null;

                if (maxWidth != null)
                {
                    widthScale = maxWidth / srcWidth;
                }
                if (maxHeight != null)
                {
                    heightScale = maxHeight / srcHeight;
                }

                var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
                return {
                    width: Math.round(srcWidth * ratio),
                    height: Math.round(srcHeight * ratio)
                };
            }

            function getBase64FromImage(img, width, height) {
                var size = getMaxSize(width, height, 2400, 2400)
                canvas.width = size.width;
                canvas.height = size.height;
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, size.width, size.height);
                ctx.drawImage(img, 0, 0, size.width, size.height);
                return canvas.toDataURL('image/jpeg', 0.9);
            }
            img = document.getElementsByTagName('img')[0]
            console.log(img)
            res = getBase64FromImage(img, img.width, img.height)
            return res
        ''')
        if base64img:
            convert_str_for_encoding = base64img.split('base64,')[1]
            imgdata = base64.b64decode(convert_str_for_encoding)

            old_name = img_url.split('/')[-1].replace('.','-')

            fold_name = ids[:4]
            directory = os.path.dirname(os.path.abspath(__file__))+'/images2/{}/'.format(fold_name)
            if not os.path.exists(directory):
                os.makedirs(directory)
                #print('FOLDER WAS CREATED')
            path = os.path.dirname(os.path.abspath(__file__))+'/images2/{}/{}-{}.jpg'.format(fold_name, ids, old_name)
            #print(f'path      ---------     {path}')
            f = open(path, 'wb')
            f.write(imgdata)
            f.close()
            return path
        else:
            return ''
    except WebDriverException as err:
        message = 'Error convert to base64 - no webdriver'
        write_error_in_file(err, message)
        print(message)

        return ''


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


def get_inner_info(bsd, url, driver):
    time.sleep(2)

    print(driver.get_log('browser')[-1]['message'])
    if bsd:
        try:
            soup = BeautifulSoup(bsd, 'html.parser')
        except TypeError:
            bsd = get_driver(url)
            get_inner_info(bsd, url, driver)


    name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
    
    name = name_full.split(' ')[0]
    second_name = name_full.split(' ')[-1]
    surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
    zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
    birth = get_value_from_field(soup, 'Дата рождения')
    voin_4astj = get_value_from_field(soup, 'Воинская часть')
    poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
    mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
    mesto_priziva = get_value_from_field(soup, 'Место призыва')
    data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
    data_smerti = get_value_from_field(soup, 'Дата смерти')
    some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
    page_url = str(url)
    komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''


    # REWARD
    naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
    data_podviga = get_value_from_field(soup, 'Даты подвига')
    data_vibitija = get_value_from_field(soup, 'Дата выбытия')
    pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
    mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
    kto_nagradil = get_value_from_field(soup, 'Кто наградил')
    isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
    nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
    nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
    nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
    archive = get_value_from_field(soup, 'Архив')
    mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
    nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
    tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
    nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
    data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
    prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
    prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
    izdan = get_reward_info(soup, 'Издан:')
    fond = get_reward_info(soup, 'Фонд:')
    opisj = get_reward_info(soup, 'Опись:')
    edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
    nomer_zapisi = get_reward_info(soup, '№ записи:')

    if 'chelovek_nagrazhdenie' in str(url):
        ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
    elif '_donesenie' in str(url):
        ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
    elif 'chelovek_zahoronenie' in str(url):
        ids = re.findall(r"(?<=chelovek_zahoronenie)[^\/]+", str(url))[0]
    elif 'chelovek_kartoteka_memorial' in str(url):
        ids = re.findall(r"(?<=chelovek_kartoteka_memorial)[^\/]+", str(url))[0]
    elif 'chelovek_yubileinaya_kartoteka' in str(url):
        ids = re.findall(r"(?<=chelovek_yubileinaya_kartoteka)[^\/]+", str(url))[0]
    elif 'chelovek_kartoteka' in str(url):
        ids = re.findall(r"(?<=chelovek_kartoteka)[^\/]+", str(url))[0]
    elif 'chelovek_gospital' in str(url):
        ids = re.findall(r"(?<=chelovek_gospital)[^\/]+", str(url))[0]
    elif '_knigi_pamyati' in str(url):
        ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
    elif 'chelovek_vpp' in str(url):
        ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
    elif 'chelovek_plen' in str(url):
        ids = re.findall(r"(?<=chelovek_plen)[^\/]+", str(url))[0]
    elif 'chelovek_prikaz' in str(url):
        ids = re.findall(r"(?<=chelovek_prikaz)[^\/]+", str(url))[0]
    elif 'chelovek_predstavlenie' in str(url):
        ids = re.findall(r"(?<=chelovek_predstavlenie)[^\/]+", str(url))[0]

    else:
        print(url)
        ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
        full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
        name = full_nm.split(' ')[1]
        second_name = full_nm.split(' ')[-1]
        surname = full_nm.split(' ')[0]


    try:
        image_reward = get_image_reward(soup, driver, ids)
        print(f'image reward   -  {image_reward}')
    except JavascriptException as err:
        write_error_in_file(err)
        image_reward = ''

    try:
        image_general = get_image(soup, driver, ids)
    except JavascriptException: 
        write_error_in_file(err) 
        image_general = ''

    try:
        image_medal = get_image_medal(soup, driver, ids)
        print(f'image medal   -    {image_medal}')
    except JavascriptException:    
        write_error_in_file(err)
        image_medal = ''

    lal = soup.find_all('div', {"class":"other_rewards_item_block"})
    if lal:
        main_domain = 'https://pamyat-naroda.ru'
        links = [ i.find('a').get('href') for i in lal ]
        print(links)
        for i in links:
            new_u = main_domain+i
            time.sleep(4)
            driver.get(new_u)
            cursor, conn = get_inner_info(driver.page_source, new_u, cursor, conn, driver)
            

    print('DONE!!!!!!')
    





def save_pid(driver):
    pid = driver.service.process.pid
    p = psutil.Process(driver.service.process.pid)
    pid2 = p.children(recursive=True)

    pid2_g = [i.pid for i in pid2]
    pid2_g.append(pid)

    return pid, pid2, pid2_g

def write_error_in_file(err, message=False):
    f = open("stdout_error.txt", "a+")
    if message:
        f.write(message+'\n')
    f.write(str(datetime.now())+'  '+str(err))
    f.write('\n\n')
    f.close()





def main():

    # Set screen resolution
    display = Display(visible=0, size=(1920, 800))
    display.start()

    base_url = "https://pamyat-naroda.ru/heroes/podvig-chelovek_kartoteka1001712004/?backurl=%2Fheroes%2F%3Fadv_search%3Dy%26last_name%3D%26first_name%3D%26middle_name%3D%26date_birth%3D31.12.1895%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D867"
    # base_url = "https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie27373768/?backurl=%2Fheroes%2F%3Flast_name%3D%26first_name%3D%26middle_name%3D%26date_birth%3D31.12.1910%26adv_search%3Dy%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D1"
    #base_url = "https://pamyat-naroda.ru/heroes/podvig-chelovek_kartoteka1001712004/?backurl=%2Fheroes%2F%3Fadv_search%3Dy%26last_name%3D%26first_name%3D%26middle_name%3D%26date_birth%3D31.12.1895%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D867"
    #base_url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie27373768/?backurl=%2Fheroes%2F%3Flast_name%3D%26first_name%3D%26middle_name%3D%26date_birth%3D31.12.1910%26adv_search%3Dy%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D1'
    driver = get_driver(base_url)

    get_inner_info(driver.page_source, base_url, driver)
    #soup = BeautifulSoup(driver, 'html.parser')
    #print(soup)
    #lal = soup.find_all('div', class_='other_rewards_item_block')
    #links = [ i.find('a').get('href') for i in links ]
    #print(links)
    # quit Xvfb display
    display.stop()



if __name__ == '__main__':
    main()
  
