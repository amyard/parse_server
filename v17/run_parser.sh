#!/bin/bash
echo -e "\n$(date "+%d-%m-%Y --- %T") --- Starting work\n"

source /RAID/v1/.venv/bin/activate
kill -9 `lsof -t -i:7575`
celery -A tasks flower --port=7575 &
celery -A tasks worker --concurrency 2 --loglevel=info &
python3 /RAID/v1/pamjatj_parse_url.py && fg


echo -e "\n$(date "+%T") \t Script Terminated"
