# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
os.system("taskset -p 0xff %d" % os.getpid())
from celery import Celery
 

import requests
import time
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchWindowException, InvalidSessionIdException, WebDriverException, JavascriptException

from pyvirtualdisplay import Display

import psycopg2
import pandas as pd
from datetime import datetime, timedelta
import base64
import psutil
import subprocess


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth={}&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_vpp&page=1'.format(date)


def get_driver(url):
    options = Options()
    options.add_argument("user-data-dir=/opt/google/chrome")
    options.add_argument('--profile-directory=Default')


    options.add_argument("no-sandbox")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--headless')
    

    options.add_argument("--disable-extensions")
    options.add_argument("--start-maximized")
    options.add_argument("--disable-infobars")
    options.add_experimental_option('useAutomationExtension', False)

    #options.add_argument("--disable-gpu")              # applicable to windows os only



    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        #executable_path="/usr/bin/chromedriver",
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/ch_drv/chromedriver_78_3',
        desired_capabilities=caps,
        chrome_options = options,
    )
    
    driver.set_window_size(1920, 800)
    driver.maximize_window()
    driver.get(url)

    time.sleep(2)
    return driver



# GENERAL PHOTO
def get_image(soup, driver, ids):
    look = soup.find('div', class_='gi__img')
    if look:
        img_src = look.find('img').get('src') if look else ''
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# MAIN DOCUMENT PICT
def get_image_reward(soup, driver, ids):
    look = soup.find_all("img", {'class':'mapster_el'})
    if look:
        img_src = soup.find_all("img", {'class':'mapster_el'})[0].get('src')
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# PHOTO OF MEDAL
def get_image_medal(soup, driver, ids):
    look = soup.find_all("div", {'class':'c-reward__img'})
    if look:
        img_src = soup.find_all("div", {'class':'c-reward__img'})[0].find('img').get('src')
        if 'bitrix' in img_src:
            img_src = 'https://pamyat-naroda.ru'+img_src
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


def save_image_on_server(img_url, driver, ids):


    try:
        driver.get(img_url)

        base64img = driver.execute_script('''
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
                var widthScale = null;
                var heightScale = null;

                if (maxWidth != null)
                {
                    widthScale = maxWidth / srcWidth;
                }
                if (maxHeight != null)
                {
                    heightScale = maxHeight / srcHeight;
                }

                var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
                return {
                    width: Math.round(srcWidth * ratio),
                    height: Math.round(srcHeight * ratio)
                };
            }

            function getBase64FromImage(img, width, height) {
                var size = getMaxSize(width, height, 2400, 2400)
                canvas.width = size.width;
                canvas.height = size.height;
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, size.width, size.height);
                ctx.drawImage(img, 0, 0, size.width, size.height);
                return canvas.toDataURL('image/jpeg', 0.9);
            }
            img = document.getElementsByTagName('img')[0]
            console.log(img)
            res = getBase64FromImage(img, img.width, img.height)
            return res
        ''')
        if base64img:
            convert_str_for_encoding = base64img.split('base64,')[1]
            imgdata = base64.b64decode(convert_str_for_encoding)

            old_name = img_url.split('/')[-1].replace('.','-')

            fold_name = ids[:4]
            directory = os.path.dirname(os.path.abspath(__file__))+'/images2/{}/'.format(fold_name)
            if not os.path.exists(directory):
                os.makedirs(directory)
            path = os.path.dirname(os.path.abspath(__file__))+'/images2/{}/{}-{}.jpg'.format(fold_name, ids, old_name)
            f = open(path, 'wb')
            f.write(imgdata)
            f.close()
            return path
        else:
            return ''
    except WebDriverException as err:
        message = 'Error convert to base64 - no webdriver'
        write_error_in_file(err, message)
        print(message)

        return ''


def get_new_img(bsd, url, cursor, conn, driver, pid, pid2, ids):
    
    print(url)
    print('Start webdriver')

    if bsd:
        try:
            soup = BeautifulSoup(bsd, 'html.parser')
        except TypeError:
            bsd = get_driver(url)
            get_new_img(bsd, url, cursor, conn, driver, pid, pid2, ids)

    print('Finish webdriver')

    try:
        image_reward = get_image_reward(soup, driver, ids)
    except JavascriptException as err:
        write_error_in_file(err)
        image_reward = ''

    try:
        image_general = get_image(soup, driver, ids)
    except JavascriptException: 
        write_error_in_file(err) 
        image_general = ''

    try:
        image_medal = get_image_medal(soup, driver, ids)
    except JavascriptException:    
        write_error_in_file(err)
        image_medal = ''

   
    print('start update img path')
    # update data in query
    sql_update_query = """Update parsed_data set image_reward = %s, image_general = %s, image_medal=%s where page_url = %s"""
    cursor.execute(sql_update_query, (image_reward, image_general, image_medal, url))
    conn.commit()
    
    print('finish update img path')
    print(image_reward)
    print(image_general)
    print(image_medal)


    # cursor.close()
    # conn.close()

    # kill pid1
    p = psutil.Process(pid)
    p.kill()

    # kill pid2
    for i in pid2:
        try:
            i.kill()
        except psutil.NoSuchProcess as err:
            message = 'No PID for process'
            write_error_in_file(err, message)
    
    print('#'*150)
    print('DONE!!!!')
    print('#'*150)



def save_pid(driver):
    pid = driver.service.process.pid
    p = psutil.Process(driver.service.process.pid)
    pid2 = p.children(recursive=True)

    pid2_g = [i.pid for i in pid2]
    pid2_g.append(pid)

    return pid, pid2, pid2_g




def delete_img(path):
    if os.path.exists(path):
        os.remove(path) 


def save_pid(driver):
    pid = driver.service.process.pid
    p = psutil.Process(driver.service.process.pid)
    pid2 = p.children(recursive=True)

    pid2_g = [i.pid for i in pid2]
    pid2_g.append(pid)

    return pid, pid2, pid2_g


def write_error_in_file(err, message=False):
    f = open("stdout_error.txt", "a+")
    if message:
        f.write(message+'\n')
    f.write(str(datetime.now())+'  '+str(err))
    f.write('\n\n')
    f.close()


def main():

    # TODO - изменить данные БД на те, которые на сервере.
    conn = psycopg2.connect(
                database = "kniga_test",
                user = "delme",
                password = "zaza1234",
                host = "localhost",
                port = "5432"
    )
    cursor = conn.cursor()


    # Set screen resolution
    display = Display(visible=0, size=(1920, 800))
    display.start()

    for i in range(5):
        cursor.execute(
            "select ids, page_url, image_reward, image_general, image_medal from parsed_data where image_reward <> '' and image_reward ~ '^[/images/].*' offset random() * (select count(ids) from parsed_data where image_reward <> '' and image_reward ~ '^[/images/].*') limit 1")
        data = cursor.fetchall()

        ids = data[0][0]
        url = data[0][1]
        image_reward = data[0][2]
        image_general = data[0][3]
        image_medal = data[0][4]
     
        print('Start Delete img')
        delete_img(image_reward)
        delete_img(image_general)
        delete_img(image_medal)
        print('Finish delete img')


        try:
            driver = get_driver(url)

            pid, pid2, pid2_g = save_pid(driver)

            get_new_img(driver.page_source, url, cursor, conn, driver, pid, pid2, ids)
        except WebDriverException as err:
            driver.close()

            message = 'Some problem with chromedriver'
            write_error_in_file(err)
            print(message)
            driver = get_driver(url)
            time.sleep(5)

            pid, pid2, pid2_g = save_pid(driver)

            driver = get_driver(url)
            get_new_img(driver.page_source, url, cursor, conn, driver, pid, pid2, ids)
            
        except NoSuchWindowException as err:
            driver.close()
            
            message = 'Main driver not worked from tasks'
            write_error_in_file(err)
            print(message)
            driver = get_driver(url)
            time.sleep(5)

            pid, pid2, pid2_g = save_pid(driver)


            driver = get_driver(url)
            get_new_img(driver.page_source, url, cursor, conn, driver, pid, pid2, ids)

        except InvalidSessionIdException as err:
            driver.close()

            message = 'NO session ID'
            write_error_in_file(err)
            print(message)

            driver = get_driver(url)
            time.sleep(5)

            pid, pid2, pid2_g = save_pid(driver)
            
            driver = get_driver(url)
            get_new_img(driver.page_source, url, cursor, conn, driver, pid, pid2, ids)


    # base_url = 'https://pamyat-naroda.ru/'
    # driver = get_driver(base_url)

    # коннект к БД
    # запрос на получение юрл и пути картинки
    # удаляем картинки - делаем проверку на наличие картинки вначалеЖ и наличие картинки после удаления
    # в функцию отправляем юрл - где будет коннект и парсинг новых картинок 
    # меняем пути картинок на новые


    driver.close()
    if (conn):
            conn.close()
            print("PostgreSQL connection is closed")


    # quit Xvfb display
    display.stop()



if __name__ == '__main__':
    main()
  
