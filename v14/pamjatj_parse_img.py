from datetime import datetime, timedelta
from tasks import get_new_path
import time
import os

def main():
    now = datetime.now()
    one_year = datetime.now() + timedelta(days=365)
    while now < one_year:
        get_new_path.delay()
        time.sleep(1)


        min = datetime.now()
        if min.minute % 30 == 0 and min.second > 40:
             os.system("pkill run_single_parser.sh")

if __name__ == '__main__':
    main()
