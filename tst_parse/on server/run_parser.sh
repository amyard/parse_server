#!/bin/bash
echo -e "\n$(date "+%d-%m-%Y --- %T") --- Starting work\n"

source .venv/bin/activate
kill -9 `lsof -t -i:7575`
celery -A tasks flower --port=7575 &
celery -A tasks worker --concurrency 16 --loglevel=info & 
python /home/felix/work/tst_parse/pamjatj_main.py && fg


echo -e "\n$(date "+%T") \t Script Terminated"