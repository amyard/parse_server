#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import requests
import os
import time
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import WebDriverException

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pyvirtualdisplay import Display

import psycopg2
import pandas as pd
from datetime import datetime, timedelta
import base64

from tasks import get_inner_info_task


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)

############################################
##########       SERVER DRIVER     #########
############################################
# def get_driver(url):


#     options = Options()
#     options.add_argument("user-data-dir=/opt/google/chrome")
#     options.add_argument('--profile-directory=Default')
#     options.add_argument("no-sandbox")
#     caps = DesiredCapabilities().CHROME
#     caps["pageLoadStrategy"] = "eager"
    
#     driver = webdriver.Chrome(
#         executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
#         desired_capabilities=caps,
#         chrome_options = options,
# 	# options = options
#     )
#     driver.set_window_size(1920, 800)
#     driver.maximize_window()

#     driver.get(url)

#     time.sleep(2)
#     return driver



############################################
##########       LOCAL DRIVER      #########
############################################

def get_driver(url):


    options = Options()
    options.add_argument('--disable-gpu')
    options.add_argument('--log-level=3')
    # options.add_argument("user-data-dir=/opt/google/chrome")
    # options.binary_location = r"/usr/bin/google-chrome-stable"
    options.add_argument("no-sandbox")
    options.add_argument("--headless")
    options.add_argument('--profile-directory=Default')

    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
        desired_capabilities=caps,
        chrome_options = options,
    )
    driver.set_window_size(1920, 800)
    driver.maximize_window()

    driver.get(url)

    time.sleep(2)
    return driver



def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    look = soup.find('div', class_='heroes-result-total')
    if look:
        res = look.find('span').text.strip().replace(' ', '')
        res = int(res) if res != "" else 0
    else:
        res = 0
    
    look2 = soup.find('ul', class_='pagination-list')
    if look2:
        try:
            pagg = look2.find_all('a')[-1].get('onclick')
            last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
        except IndexError:
            if res < 10:
                last_page = 1
            else:
                print('Houston we have a problem. {}'.format(driver.current_url))
                get_total_pages(driver)

    return int(last_page), int(res)



def main():

    # get list of dates for parsing
    # при выборе дабы отображаются данные за год
    dd = []
    date_index = pd.date_range(start='18800101', end='20190101', freq='Y')
    dd = [ i.strftime('%d.%m.%Y') for i in date_index ]


    # TODO - изменить данные БД на те, которые на сервере.
    conn = psycopg2.connect(
                database = "kniga", 
                user = "delme", 
                password = "zaza1234", 
                host = "localhost", 
                port = "5432")
    cursor = conn.cursor()


    # create table if not exists - general info
    q1 = """CREATE TABLE IF NOT EXISTS "parsed_data" 
                    ("ids" text NOT NULL,
                    "name" text NOT NULL,
                    "second_name" text NOT NULL,
                    "surname" text NOT NULL,
                    "zvanie" text NOT NULL,
                    "birth" text NOT NULL,
                    "voin_4astj" text NOT NULL,
                    "poslednee_mesto_slyzhbi" text NOT NULL,
                    "mesto_birth" text NOT NULL,
                    "mesto_priziva" text NOT NULL,
                    "data_postyplenija_na_slyzhby" text NOT NULL,
                    "data_smerti" text NOT NULL,
                    "some_info_about_general" text NOT NULL,
                    "image_general" text NOT NULL,
                    "page_url" text NOT NULL,
                    "komandyy4ij_voinsk_4astjami" text NOT NULL,


                    "naimenovanie_nagradi" text NOT NULL,
                    "data_podviga" text NOT NULL,
                    "data_vibitija" text NOT NULL,
                    "pri4ina_vibitija" text NOT NULL,
                    "mesto_vibitija" text NOT NULL,
                    "kto_nagradil" text NOT NULL,
                    "isto4nik_informacii" text NOT NULL,
                    "nomer_fonda_ist04_informacii" text NOT NULL,
                    "nomer_opisi_ist04_informacii" text NOT NULL, 
                    "nomer_dela_isto4_informacii" text NOT NULL,
                    "archive" text NOT NULL,
                    "mesto_zahoronenija" text NOT NULL,
                    "nomer_donesenija" text NOT NULL,
                    "tip_donesenija" text NOT NULL,
                    "nazvanie_4asti" text NOT NULL,
                    "data_donesenija" text NOT NULL,
                    "image_reward" text NOT NULL,
                    "image_medal" text NOT NULL,
                    "prikaz_podrazdelenija_nmb" text NOT NULL,
                    "prikaz_podrazdelenija_ot" text NOT NULL,
                    "izdan" text NOT NULL,
                    "fond" text NOT NULL,
                    "opisj" text NOT NULL,
                    "edinica_hranenija" text NOT NULL,
                    "nomer_zapisi" text NOT NULL)
        """
    cursor.execute(q1)
    conn.commit()

    # create table if not exists - parsed pages
    q2 = """CREATE TABLE IF NOT EXISTS "pages_in_use"
                ("year" varchar(255) NOT NULL,
                "total_pages" varchar(255) NOT NULL,
                "page_in_use" varchar(255) NOT NULL,
                "url_page" text NOT NULL,
                "url_current_inner" text NOT NULL,
                "parsed" boolean NOT NULL DEFAULT false)
    """ 
    cursor.execute(q2)
    conn.commit()

    # Set screen resolution 
    display = Display(visible=0, size=(1920, 800))
    display.start()

    base_url = 'https://pamyat-naroda.ru/'
    driver = get_driver(base_url)

    # получаем список url из таблици
    for i_date in dd:
        driver.get(get_date_url(i_date))
        time.sleep(5)

        last_elem, total_elem = get_total_pages(driver)
        

        # итерирую по всем страницам из данного года
        for page_nmb in range(1, 2 if last_elem == 1 else last_elem):
            
            page_link = 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page={}&data_vibitiya={}'.format(page_nmb, i_date)

            driver.get(page_link)
            time.sleep(2)
            try:
                list_urls = table_links(driver.page_source)
            except WebDriverException:
                # selenium.common.exceptions.WebDriverException: Message: unknown error: session deleted because of page crash
                driver.get(page_link)
                time.sleep(4)
                list_urls = table_links(driver.page_source)

            # итерировать внутрению страницу
            for url in list_urls:

                # insert data in query
                insert_query = """ INSERT INTO pages_in_use 
                            (year, total_pages, page_in_use, url_page, url_current_inner) 
                            VALUES (%s,%s,%s,%s,%s)
                            """
                record_to_insert = (i_date, total_elem, page_nmb, page_link, url)
                cursor.execute(insert_query, record_to_insert)
                conn.commit()


                # CALL TASK
                get_inner_info_task.delay(url)

    driver.close()
    if (conn):
            conn.close()
            print("PostgreSQL connection is closed")


    # quit Xvfb display
    display.stop()



if __name__ == '__main__':
    main()
  
