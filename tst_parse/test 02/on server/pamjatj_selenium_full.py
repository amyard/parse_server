#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import requests
import os
import time
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pyvirtualdisplay import Display

import psycopg2
import pandas as pd
from datetime import datetime, timedelta
import base64


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)


############################################
##########       SERVER DRIVER     #########
############################################
# def get_driver(url):


#     options = Options()
#     options.add_argument("user-data-dir=/opt/google/chrome")
#     options.add_argument('--profile-directory=Default')
#     options.add_argument("no-sandbox")
#     caps = DesiredCapabilities().CHROME
#     caps["pageLoadStrategy"] = "eager"
    
#     driver = webdriver.Chrome(
#         executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
#         desired_capabilities=caps,
#         chrome_options = options,
# 	# options = options
#     )
#     driver.set_window_size(1920, 800)
#     driver.maximize_window()

#     driver.get(url)

#     time.sleep(2)
#     return driver



############################################
##########       LOCAL DRIVER      #########
############################################

def get_driver(url):


    options = Options()
    options.add_argument('--disable-gpu')
    options.add_argument('--log-level=3')
    # options.add_argument("user-data-dir=/opt/google/chrome")
    # options.binary_location = r"/usr/bin/google-chrome-stable"
    options.add_argument("no-sandbox")
    options.add_argument("--headless")
    options.add_argument('--profile-directory=Default')
    options.add_argument("user-data-dir=/opt/google/chrome")


    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
        desired_capabilities=caps,
        chrome_options = options,
    )
    driver.set_window_size(1920, 800)
    driver.maximize_window()

    driver.get(url)

    time.sleep(2)
    return driver
    


def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    look = soup.find('div', class_='heroes-result-total')
    if look:
        res = int(look.find('span').text.strip().replace(' ', ''))
    else:
        res = 0
    
    look2 = soup.find('ul', class_='pagination-list')
    if look2:
        try:
            pagg = look2.find_all('a')[-1].get('onclick')
            last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
        except IndexError:
            if res < 10:
                last_page = 1
            else:
                print('Houston we have a problem. {}'.format(driver.current_url))
                get_total_pages(driver)

    return int(last_page), int(res)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


# GENERAL PHOTO
def get_image(soup, driver, ids):
    look = soup.find('div', class_='gi__img')
    if look:
        img_src = look.find('img').get('src') if look else ''
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# MAIN DOCUMENT PICT
def get_image_reward(soup, driver, ids):
    look = soup.find_all("img", {'class':'mapster_el'})
    if look:
        img_src = soup.find_all("img", {'class':'mapster_el'})[0].get('src')
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


# PHOTO OF MEDAL
def get_image_medal(soup, driver, ids):
    look = soup.find_all("div", {'class':'c-reward__img'})
    if look:
        img_src = soup.find_all("div", {'class':'c-reward__img'})[0].find('img').get('src')
        if 'bitrix' in img_src:
            img_src = 'https://pamyat-naroda.ru'+img_src
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


def save_image_on_server(img_url, driver, ids):

    driver.get(img_url)
    time.sleep(1)

    base64img = driver.execute_script('''
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
            var widthScale = null;
            var heightScale = null;

            if (maxWidth != null)
            {
                widthScale = maxWidth / srcWidth;
            }
            if (maxHeight != null)
            {
                heightScale = maxHeight / srcHeight;
            }

            var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
            return {
                width: Math.round(srcWidth * ratio),
                height: Math.round(srcHeight * ratio)
            };
        }

        function getBase64FromImage(img, width, height) {
            var size = getMaxSize(width, height, 600, 600)
            canvas.width = size.width;
            canvas.height = size.height;
            ctx.fillStyle = 'white';
            ctx.fillRect(0, 0, size.width, size.height);
            ctx.drawImage(img, 0, 0, size.width, size.height);
            return canvas.toDataURL('image/jpeg', 0.9);
        }
        img = document.getElementsByTagName('img')[0]
        console.log(img)
        res = getBase64FromImage(img, img.width, img.height)
        return res
    ''')

    convert_str_for_encoding = base64img.split('base64,')[1]
    imgdata = base64.b64decode(convert_str_for_encoding)

    old_name = img_url.split('/')[-1].replace('.','-')
    path = os.path.dirname(os.path.abspath(__file__))+'/images/{}-{}.jpg'.format(ids, old_name)
    f = open(path, 'wb')
    f.write(imgdata)
    f.close()
    return path


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


def get_inner_info(bsd, url, cursor, conn, driver):

    soup = BeautifulSoup(bsd, 'html.parser')


    name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
    
    name = name_full.split(' ')[0]
    second_name = name_full.split(' ')[-1]
    surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
    zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
    birth = get_value_from_field(soup, 'Дата рождения')
    voin_4astj = get_value_from_field(soup, 'Воинская часть')
    poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
    mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
    mesto_priziva = get_value_from_field(soup, 'Место призыва')
    data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
    data_smerti = get_value_from_field(soup, 'Дата смерти')
    some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
    page_url = str(url)
    komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''


    # REWARD
    naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
    data_podviga = get_value_from_field(soup, 'Даты подвига')
    data_vibitija = get_value_from_field(soup, 'Дата выбытия')
    pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
    mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
    kto_nagradil = get_value_from_field(soup, 'Кто наградил')
    isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
    nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
    nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
    nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
    archive = get_value_from_field(soup, 'Архив')
    mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
    nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
    tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
    nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
    data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
    prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
    prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
    izdan = get_reward_info(soup, 'Издан:')
    fond = get_reward_info(soup, 'Фонд:')
    opisj = get_reward_info(soup, 'Опись:')
    edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
    nomer_zapisi = get_reward_info(soup, '№ записи:')


    if 'chelovek_nagrazhdenie' in str(url):
        ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
    elif '_donesenie' in str(url):
        ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
    elif 'chelovek_zahoronenie' in str(url):
        ids = re.findall(r"(?<=chelovek_zahoronenie)[^\/]+", str(url))[0]
    elif 'chelovek_gospital' in str(url):
        ids = re.findall(r"(?<=chelovek_gospital)[^\/]+", str(url))[0]
    elif '_knigi_pamyati' in str(url):
        ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
    elif 'chelovek_vpp' in str(url):
        ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
    else:
        ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
        full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
        name = full_nm.split(' ')[1]
        second_name = full_nm.split(' ')[-1]
        surname = full_nm.split(' ')[0]

    image_reward = get_image_reward(soup, driver, ids)
    image_general = get_image(soup, driver, ids)
    image_medal = get_image_medal(soup, driver, ids)

    # insert data in query
    insert_query = """ INSERT INTO parsed_data 
                (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                
                
                naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                izdan, fond, opisj, edinica_hranenija, nomer_zapisi) 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                        %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                """
    record_to_insert = (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                        poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                        data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                        
                        naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                        kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                        nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                        nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                        izdan, fond, opisj, edinica_hranenija, nomer_zapisi)
    cursor.execute(insert_query, record_to_insert)
    conn.commit()


    # update data in query
    insert_query2 = """Update pages_in_use set parsed = true where url_current_inner = %s"""

    cursor.execute(insert_query2, (url,))
    conn.commit()



    print (ids, " - Record inserted successfully into table")

def main():

    # get list of dates for parsing
    # при выборе дабы отображаются данные за год
    dd = []
    date_index = pd.date_range(start='18800101', end='20190101', freq='Y')
    dd = [ i.strftime('%d.%m.%Y') for i in date_index ]


    # TODO - изменить данные БД на те, которые на сервере.
    conn = psycopg2.connect(
                database = "kniga", 
                user = "delme", 
                password = "zaza1234", 
                host = "localhost", 
                port = "5432")
    cursor = conn.cursor()


    # create table if not exists - general info
    q1 = """CREATE TABLE IF NOT EXISTS "parsed_data" 
                    ("ids" text NOT NULL,
                    "name" text NOT NULL,
                    "second_name" text NOT NULL,
                    "surname" text NOT NULL,
                    "zvanie" text NOT NULL,
                    "birth" text NOT NULL,
                    "voin_4astj" text NOT NULL,
                    "poslednee_mesto_slyzhbi" text NOT NULL,
                    "mesto_birth" text NOT NULL,
                    "mesto_priziva" text NOT NULL,
                    "data_postyplenija_na_slyzhby" text NOT NULL,
                    "data_smerti" text NOT NULL,
                    "some_info_about_general" text NOT NULL,
                    "image_general" text NOT NULL,
                    "page_url" text NOT NULL,
                    "komandyy4ij_voinsk_4astjami" text NOT NULL,


                    "naimenovanie_nagradi" text NOT NULL,
                    "data_podviga" text NOT NULL,
                    "data_vibitija" text NOT NULL,
                    "pri4ina_vibitija" text NOT NULL,
                    "mesto_vibitija" text NOT NULL,
                    "kto_nagradil" text NOT NULL,
                    "isto4nik_informacii" text NOT NULL,
                    "nomer_fonda_ist04_informacii" text NOT NULL,
                    "nomer_opisi_ist04_informacii" text NOT NULL, 
                    "nomer_dela_isto4_informacii" text NOT NULL,
                    "archive" text NOT NULL,
                    "mesto_zahoronenija" text NOT NULL,
                    "nomer_donesenija" text NOT NULL,
                    "tip_donesenija" text NOT NULL,
                    "nazvanie_4asti" text NOT NULL,
                    "data_donesenija" text NOT NULL,
                    "image_reward" text NOT NULL,
                    "image_medal" text NOT NULL,
                    "prikaz_podrazdelenija_nmb" text NOT NULL,
                    "prikaz_podrazdelenija_ot" text NOT NULL,
                    "izdan" text NOT NULL,
                    "fond" text NOT NULL,
                    "opisj" text NOT NULL,
                    "edinica_hranenija" text NOT NULL,
                    "nomer_zapisi" text NOT NULL)
        """
    cursor.execute(q1)
    conn.commit()

    # create table if not exists - parsed pages
    q2 = """CREATE TABLE IF NOT EXISTS "pages_in_use"
                ("year" varchar(255) NOT NULL,
                "total_pages" varchar(255) NOT NULL,
                "page_in_use" varchar(255) NOT NULL,
                "url_page" text NOT NULL,
                "url_current_inner" text NOT NULL,
                "parsed" boolean NOT NULL DEFAULT false)
    """ 
    cursor.execute(q2)
    conn.commit()

    # Set screen resolution 
    display = Display(visible=0, size=(1920, 800))
    display.start()

    base_url = 'https://pamyat-naroda.ru/'
    driver = get_driver(base_url)

    # получаем список url из таблици
    for i_date in dd:
        driver.get(get_date_url(i_date))
        time.sleep(5)

        last_elem, total_elem = get_total_pages(driver)
        

        # итерирую по всем страницам из данного года
        for page_nmb in range(1, 2 if last_elem == 1 else last_elem):
            
            page_link = 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page={}&data_vibitiya={}'.format(page_nmb, i_date)

            driver.get(page_link)
            time.sleep(2)

            list_urls = table_links(driver.page_source)

            # итерировать внутрению страницу
            for url in list_urls:

                # insert data in query
                insert_query = """ INSERT INTO pages_in_use 
                            (year, total_pages, page_in_use, url_page, url_current_inner) 
                            VALUES (%s,%s,%s,%s,%s)
                            """
                record_to_insert = (i_date, total_elem, page_nmb, page_link, url)
                cursor.execute(insert_query, record_to_insert)
                conn.commit()


                driver.get(url)
                time.sleep(3)
                # get_inner_info(driver.page_source, url, cursor, conn, driver)

    driver.close()
    if (conn):
            # cursor.close()
            conn.close()
            print("PostgreSQL connection is closed")


    # quit Xvfb display
    display.stop()



if __name__ == '__main__':
    main()
  
