from datetime import datetime, timedelta
from tasks import clean_pid, get_inner_info_task
import psycopg2
import time

def main():
    clean_pid.delay()

    now = datetime.now()
    one_year = datetime.now() + timedelta(days=365)
    while now < one_year:
        conn = psycopg2.connect(
            database="kniga",
            user="delme",
            password="zaza1234",
            host="localhost",
            port="5432")
        cursor = conn.cursor()
        # cursor.execute("select url_current_inner from pages_in_use where parsed = false OFFSET floor(random()*1) LIMIT 1")
        cursor.execute(
            "select url_current_inner from pages_in_use where parsed = false and url_current_inner <> ''  offset random() * (select count(*) from pages_in_use) limit 1 ")
        data = cursor.fetchall()
        if data: 
#       try:
            url = data[0][0]
            get_inner_info_task.delay(url)
        else:
            continue

#        except IndexError:
#            print(data)


#        url = data[0][0]
#        get_inner_info_task.delay(url)

        cursor.close()
        conn.close()
        time.sleep(1)



if __name__ == '__main__':
    main()
